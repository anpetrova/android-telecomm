package npcompete.telecorp;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.Cursor;
import android.content.Context;
import android.content.ContentValues;

import java.lang.reflect.Member;

/**
 * Created by Luis Arriaga on 1/27/2015.
 */

public class MyDBHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_Name = "names.db";
    private static final String DATABASE_Email = "emails.db";
    private static final String DATABASE_Pass = "passwords.db";
    public static final String TABLE_MEMBERS = "members";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_Name = "names";
    private static final String COLUMN_Email = "emails";
    private static final String COLUMN_Pass = "passwords";

    public MyDBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_Email+DATABASE_Pass+DATABASE_Name, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE" + TABLE_MEMBERS + "(" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT " +
                COLUMN_Name + "TEXT" +
                COLUMN_Email + " TEXT " +
                COLUMN_Pass + " TEXT " +
                ")";
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS" + TABLE_MEMBERS);
        onCreate(db);
    }

    //Add new row to db
    public void addMember(Members name, Members email, Members pass){
        ContentValues values = new ContentValues();
        values.put(COLUMN_Name, name.get_name());

        values.put(COLUMN_Email, email.get_email());
        values.put(COLUMN_Pass, pass.get_pass());
        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_MEMBERS, null, values);
        db.close();
    }

    //Delete member from database
    public void deleteMember(String name, String email, String pass){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM" + TABLE_MEMBERS + "WHERE" + COLUMN_Name + "=\"" + name + "\";");

        db.execSQL("DELETE FROM" + TABLE_MEMBERS + "WHERE" + COLUMN_Email + "=\"" + email + "\";");
        db.execSQL("DELETE FROM" + TABLE_MEMBERS + "WHERE" + COLUMN_Pass + "=\"" + pass + "\";");
    }

    //Print out db as string
    public String databaseToString(){
        String dbString = "";
        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT * FROM" + TABLE_MEMBERS + "WHERE 1";

        //Cursor point to a location in your results
        Cursor c = db.rawQuery(query, null);
        //Move to first row in table
        c.moveToFirst();

        while(!c.isAfterLast()) {
            if (c.getString(c.getColumnIndex("name")) != null) {
                dbString += c.getString(c.getColumnIndex("name"));
                dbString += "\n";
            }
            if (c.getString(c.getColumnIndex("email")) != null) {
                dbString += c.getString(c.getColumnIndex("email"));
                dbString += "\n";
            }
            if (c.getString(c.getColumnIndex("pass")) != null) {
                dbString += c.getString(c.getColumnIndex("pass"));
                dbString += "\n";
            }
        }
        db.close();
        return dbString;
    }
}

